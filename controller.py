import pandas as pd


class Controller:
    def __init__(self, regex_gen):
        self.generator = regex_gen

    def feed_samples(self, method='stdin|manual'):
        feeder = set()
        methods = method.split('|')
        if 'manual' in methods:
            if 'stdin' in methods:
                print('Leave blank to stop')
                while True:
                    inp = input('From: ')
                    out = input('To: ')
                    if not (inp and out):
                        break
                    feeder.add((inp, out))
            elif 'args' in methods:
                arg_pairs = zip(methods[2::2], methods[3::2])
                for pair in arg_pairs:
                    feeder.add(pair)

        elif 'csv' in methods:
            file_path = (input('CSV path: ')
                         if 'stdin' in methods
                         else methods[3])
            try:
                in_file = open(file_path, 'r')
            except Exception as E:
                print('Something went wrong and raised', E)
                return
            in_spreadsheet = pd.read_csv(in_file)
            feeder = set(
                tuple(map(str, row[1:]))
                for row in in_spreadsheet.values
            )
            in_file.close()

        print('Feeding data...')
        self.generator.train(feeder)
        print('Amount of known regex pairs:', len(self.generator.known_regex_pairs))

    def match(self, method='stdin|manual'):
        methods = method.split('|')
        if 'manual' in methods:
            if 'stdin' in methods:
                print('Leave blank to stop')
                while True:
                    inp = input('Source: ')
                    if not inp:
                        break
                    result = self.generator.match(inp)
                    if result:
                        pair, result = result
                        print('Matcher:', pair[0])
                        print('Replacer:', pair[1])
                        print('Result:', result)
                    else:
                        print('No suitable regex pair in the database to use')
                        answer = input('Generate regex now? Y/~Y: ').upper()
                        if answer == 'Y':
                            out = input('Expected result: ')
                            if not self.generator.check(out):
                                print('This result is invalid')
                                continue
                            self.generator.train(((inp, out),))
                            pair, _ = self.generator.match(inp)
                            print('Matcher:', pair[0])
                            print('Replacer:', pair[1])
            elif 'args' in methods:
                for inp in methods[2:]:
                    result = self.generator.match(inp)
                    if result:
                        pair, result = result
                        print("{}\n{}\n{}\n{}".format(inp, pair[0], pair[1], result))
                        print("~~~~~")
                    else:
                        print("{}\n\n\n".format(inp))
                        print("~~~~~")
        elif 'csv' in methods:
            file_path = input('CSV path: ') if 'stdin' in methods else methods[2]
            try:
                in_file = open(file_path, 'r')
            except Exception as E:
                print('Something went wrong and raised', E)
                return
            in_spreadsheet = pd.read_csv(in_file)
            list_of_tuples = list(
                tuple(map(str, row[1:]))
                for row in in_spreadsheet.values
            )
            successful_results = set()
            failed_results = set()

            for inp, _ in list_of_tuples:
                result = self.generator.match(inp)
                if result:
                    pair, result = result
                    successful_results.add((inp, pair[0], pair[1], result))
                else:
                    failed_results.add(inp)

            successful_results = pd.DataFrame(list(successful_results))
            failed_results = pd.DataFrame(list(failed_results))

            if len(successful_results):
                successful_results.to_csv(file_path + '.success.csv')
                print('Successful transformations have been written into', file_path + '.success.csv')
            if len(failed_results):
                failed_results.to_csv(file_path + '.failure.csv')
                print('Failed transformations have been written into', file_path + '.failure.csv')
                print('Further training is needed')
