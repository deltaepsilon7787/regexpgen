import argparse
import pickle
import sys

from controller import Controller
from regex_generator import RegexGenerator

regex_generator = None


def load_regex_generator(state_file='./state.bin'):
    global regex_generator

    try:
        with open(state_file, 'rb') as f:
            regex_generator = pickle.load(f)
    except IOError:
        regex_generator = RegexGenerator()

    return regex_generator


def save_regex_generator(state_file='./state_bin'):
    global regex_generator

    with open(state_file, 'wb') as f:
        pickle.dump(regex_generator, f)


def validate_directly(value):
    try:
        result = regex_generator.match(value)
    except AttributeError:
        print("RegexGenerator is not defined")
        raise

    if result:
        _, converted = result
        return converted
    sys.stderr.write('{} has no candidates'.format(value))
    return value


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate regex')

    parser.add_argument('--action',
                        choices=['train', 'generate'],
                        help='What to do?')
    parser.add_argument('--input', choices=['manual', 'csv'],
                        help='How many entries to process and where to get them from?')
    parser.add_argument('--method',
                        choices=['stdin', 'args'],
                        help='Source of input')
    parser.add_argument('--state_file',
                        default='./state.bin',
                        help='State file to pull/push data to. Default: state.bin')
    parser.add_argument('arguments', metavar='args', nargs='*')

    args = parser.parse_args()

    load_regex_generator(args.state_file)
    regex_controller = Controller(regex_generator)

    full_method = ''.join((
        '|'.join((args.input, args.method)),
        '|',
        '|'.join(args.arguments) if args.method == 'args' else ''
    ))
    if args.action == 'train':
        regex_controller.feed_samples(method=full_method)
    elif args.action == 'generate':
        regex_controller.match(method=full_method)

    save_regex_generator(args.state_file)
