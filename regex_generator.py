# coding: utf-8


import collections
import itertools
import operator
import re

from builtins import input
from functools import partial


class RegexGenerator:
    def __init__(self):
        self.known_regex_pairs = set()
        self.samples = set()

    @staticmethod
    def check(phone):
        """General purpose validation function for input.

        Arg:
            phone (str):
                Phone number to be matched against a certain pattern for validity.

        Returns:
            bool: Is `phone` valid?
        """
        invalid = False
        invalid |= '-' not in phone
        invalid |= (phone.count('(') + phone.count(')')) % 2 != 0
        invalid |= phone.count('(') == 0
        invalid |= phone.count(')') == 0
        invalid |= phone.count('(') / (phone.count(',') + 1) != 1
        invalid |= phone.count(')') / (phone.count(',') + 1) != 1
        invalid |= not not re.search('[^,\n]{25,}', phone)

        return not invalid

    @staticmethod
    def _check_regex_pair_applicability(pair):
        """Check if this matcher-replacer pair can be used together.

        Args:
            pair (Tuple[str, str]):
                Matcher-replacer pair to be checked if matcher has no less groups than replacer.

        Returns:
            bool: Is valid pair?
        """
        matcher, replacer = pair
        amount_of_groups = re.compile(matcher).groups
        group_id_extractor = 'g<(\d+)>'
        group_ids = re.findall(group_id_extractor, replacer) or (0,)
        group_ids = map(int, group_ids)
        last_group = max(group_ids)

        return amount_of_groups >= last_group

    @staticmethod
    def _substring_gen(orig):
        """Create a generator that yields all possible substrings in the string.

        Args:
            orig (str):
                A string to be split to a set of all substrings.

        Yields:
            str: A substring of `orig`.
        """
        string_length = len(orig)
        for i in range(string_length):
            for j in range(i + 1, string_length + 1):
                yield orig[i:j]

        return StopIteration

    @staticmethod
    def _generate_regex(orig, mod, excluded_chars=()):
        """Deterministically generate a matcher-replacer regex pair that will transform one string to another.

        Args:
            orig (str):
                Initial string.
            mod (str):
                Final string.
            excluded_chars (tuple[str]):
                Characters that may not be a part of a regex group.

        Returns:
            tuple[str, str]:
                A matcher-replacer pair.
        """
        orig_subs = RegexGenerator._substring_gen(orig)
        mod_subs = RegexGenerator._substring_gen(mod)

        length_sorted_orig_subs = sorted(
            orig_subs,
            key=len,
            reverse=True
        )
        length_sorted_mod_subs = sorted(
            mod_subs,
            key=len,
            reverse=True
        )

        def has_no_excluded_chars(source):
            return not any(char in source for char in excluded_chars)

        orig_sub_pool = (sub for sub in length_sorted_orig_subs if has_no_excluded_chars(sub))

        exists_in_LSMS = partial(operator.contains, length_sorted_mod_subs)

        existent_subs = [sub for sub in orig_sub_pool if exists_in_LSMS(sub)]

        def substring_in_existent_subs(source):
            return not any(sub != source and source in sub for sub in existent_subs)

        existent_subs_reversed = (sub for sub in reversed(existent_subs) if substring_in_existent_subs(sub))

        group_data = sorted(
            [
                (orig.find(T), len(T), T)
                for T in existent_subs_reversed
            ],
            key=operator.itemgetter(0)
        )

        groups = collections.deque()

        matching_regex = collections.deque(('^',))
        cur_index = 0
        group_num = 1

        while cur_index < len(orig):
            to_advance = False
            for ind, length, string in group_data:
                if cur_index == ind:
                    matching_regex += [
                        '(',
                        '.{',
                        str(length),
                        '})'
                    ]
                    cur_index += length
                    groups.append((group_num, len(string), string))
                    group_num += 1
                    to_advance = True
                    break
            if not to_advance:
                matching_regex.append('.')
                cur_index += 1

        matching_regex.append('$')
        matching_regex = "".join(matching_regex)

        replacer_regex = collections.deque()
        cur_index = 0

        while cur_index < len(mod):
            unique = True

            for group_num, group_length, string in groups:
                same_one = mod[cur_index:cur_index + group_length] == string
                if same_one:
                    replacer_regex.append('\\g<')
                    replacer_regex.append(str(group_num))
                    replacer_regex.append('>')
                    cur_index += group_length
                    unique = False
                    break

            if unique:
                replacer_regex.append(mod[cur_index])
                cur_index += 1

        replacer_regex = "".join(replacer_regex)

        return matching_regex, replacer_regex

    def _generalize(self):
        """A method used to prune redundant matcher-replacer regex pairs from the database."""
        matchers, replacers = list(zip(*self.known_regex_pairs))
        possible_pairs = itertools.product(set(matchers),
                                           set(replacers))
        possible_pairs = set(filter(self._check_regex_pair_applicability, possible_pairs))

        leaderboard = dict(
            (sample, set())
            for sample in self.samples
        )

        for matcher, replacer in possible_pairs:
            for original, modified in leaderboard:
                if not re.match(matcher, original):
                    continue
                if re.sub(matcher, replacer, original) == modified:
                    leaderboard[(original, modified)].add((matcher, replacer))

        result = set()
        resolved_samples = set()

        while leaderboard.keys() != resolved_samples:
            eligible_pairs = dict(
                (sample, leaderboard[sample])
                for sample in leaderboard
                if sample not in resolved_samples
            )
            all_pairs = collections.Counter(itertools.chain.from_iterable(eligible_pairs.values()))
            most_common_pair, _ = all_pairs.most_common()[0]

            for sample in eligible_pairs:
                if most_common_pair in eligible_pairs[sample]:
                    resolved_samples.add(sample)

            result.add(most_common_pair)
        self.known_regex_pairs = result

    def match(self, sample):
        """A method that iterates through all known matcher-replacer regex pairs and returns first successful pick.

        Args:
            sample (str):
                A string to be matched against known matchers.

        Returns:
            Tuple[Tuple[str, str], str]:
                A pair of matcher-replacer regex and the result of applying it to `sample`.
            None:
                If no matchers exists for this `sample`.
        """
        for pair in self.known_regex_pairs:
            matcher, replacer = pair
            if re.match(matcher, sample):
                candidate = re.sub(matcher, replacer, sample)
                if self.check(candidate):
                    return pair, candidate
        return None

    def train(self, samples):
        """A method that iterates through samples, adds generated matcher-replacer regex pairs to the database
            and generalizes them.

        Args:
            samples (Sequence[Tuple[str, str]]):
                (from_str, to_str) pairs.
        """
        self.samples |= set(samples)
        for original, modified in samples:
            pair = self._generate_regex(original, modified)
            self.known_regex_pairs.add(pair)
        self._generalize()